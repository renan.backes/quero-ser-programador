# **Projeto Quero ser um programador**

**Meus Pontos Positivos para ser um Programador:**

Creio que eu, @renan.backes, posso ser um bom programador pois ao trabalhar diariamente com linguagem SQL me identifico muito com programação, também por ter realizado o trabalho de conclusão do curso técnico programando também.
Por conta disso, acredito que o aprendizado e a vontade -- de reinventar e de certa forma contribuir, resolver problemas e facilitar trabalhos e vidas de outras pessoas -- de querer ser programador tornam-se mais evidentes.